#!/usr/bin/env bash
# cleans default sim generation directories, deletes all including results!

SCRIPT=$(readlink -f "$0")
script_path=$(dirname "$SCRIPT")

if [[ -d "$script_path/out" ]]
then
    rm -r "$script_path/out"
fi

if [[ -d "$script_path/simulation-env/tsn-sim/simulations/generated" ]]
then
    rm -r "$script_path/simulation-env/tsn-sim/simulations/generated"
fi
