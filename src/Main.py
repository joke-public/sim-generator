#!/usr/bin/env python3
import argparse
import os

from colorama import Fore
from colorama import Style

import util

SIM_ENV_PATH = "/../simulation-env/"
PARSER_PATH = "/parser/parser_main.py"
FROM_INTERMEDIATE_PATH = "/from_intermediate.py"
GEN_PATH = "/gen/Main.py"
RESULT_ANALYSIS_PATH = "/result-analysis/Main.py"
DEFAULT_OUT_PATH = "/../out"
PLOTTER_MAIN_PATH = "/plotter_main.py"


def main(topology: str, odl_log_file: str, config_file, out_dir=None, gui=False, start_plotter=False,
         no_random=False, no_raw=False):
    dir_path = os.path.dirname(os.path.realpath(__file__))

    # simulation run name, so that multiple runs have separate directories
    random_string = util.random_str(10)

    # print(f"{Fore.GREEN}sim-gen started{time} {Style.RESET_ALL}")

    # check odl log file exists
    util.ensure_is_file(odl_log_file)
    util.ensure_is_file(topology)
    if config_file:
        util.ensure_is_file(config_file)

    if not out_dir:
        out_dir = f"{dir_path}{DEFAULT_OUT_PATH}"

    if not no_random:
        out_dir = f"{out_dir}/{random_string}"

    parser_out_dir = f"{out_dir}/parser"
    result_out_dir = f"{out_dir}/result"

    # parse odl_log and topology to an intermediate file structure
    config_str = ""
    if config_file:
        config_str = f"-c {config_file}"

    print(f"{Fore.GREEN}Generating files for sim gen to {parser_out_dir}{Style.RESET_ALL}")
    print(f"Using topology         {Fore.YELLOW}{topology}{Style.RESET_ALL}")
    print(f"Using OpenDaylight log {Fore.YELLOW}{odl_log_file}{Style.RESET_ALL}")
    if config_file:
        print(f"Using config file      {Fore.YELLOW}{config_file}{Style.RESET_ALL}")
    if os.system(f"{dir_path}{PARSER_PATH} -t {topology} -l {odl_log_file} -o {parser_out_dir} {config_str}"):
        exit(1)

    if gui:
        gui_flag = "--gui"
    else:
        gui_flag = ""

    if start_plotter:
        plotter_falg = "--start-plotter"
    else:
        plotter_falg = ""

    if no_raw:
        no_raw_flag = "--no-raw"
    else:
        no_raw_flag = ""

    if os.system(f"{dir_path}{FROM_INTERMEDIATE_PATH} "
                 f"-t {topology} "
                 f"-o {result_out_dir} "
                 f"-c {parser_out_dir}/config.ini "
                 f"-i {parser_out_dir} "
                 f"--name {random_string} "
                 f"{gui_flag} {plotter_falg} {no_raw_flag}"):
        exit(1)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--topology", "-t", help="target network topology", metavar="FILE", required=True)
    parser.add_argument("--odl_log", "-l", help="odl log", metavar="FILE", required=True)
    parser.add_argument("--out", "-o", help="output directory", metavar="DIR")
    parser.add_argument("--gui", help="if the simulation should be done in the omnet++ graphical env",
                        action='store_true')
    parser.add_argument("--start-plotter", "-p", help="if the plotter should be started after the sim is finished",
                        action='store_true')
    parser.add_argument("--config", "-c", help="config file, that will overwrite defaults set by the parser",
                        metavar="FILE")
    parser.add_argument("--no-rand-out", "-nr", help="output dir should be used without creating a nested random dir",
                        action='store_true')
    parser.add_argument("--no-raw", help="doesn't save raw date", action='store_true')

    args = parser.parse_args()
    main(args.topology, args.odl_log, args.config, out_dir=args.out, gui=args.gui,
         start_plotter=args.start_plotter, no_random=args.no_rand_out, no_raw=args.no_raw)
