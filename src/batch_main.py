#!/usr/bin/env python3

import time
import argparse
import os
import glob
from pathlib import Path
import util

from colorama import Fore
from colorama import Style


def print_time_elapsed(in_time: float, title: str):
    elapsed_time = time.time() - in_time
    elapsed_time = round(elapsed_time, 3)
    print("%s %ss" % (title, elapsed_time))


MAIN = "/Main.py"


def main(topology: str, odl_log_file: str, out_dir: str, config_dir: str):
    start_time = time.time()
    dir_path = os.path.dirname(os.path.realpath(__file__))

    util.ensure_is_dir(config_dir)
    # util.ensure_is_dir(out_dir)
    util.ensure_is_file(topology)
    util.ensure_is_file(odl_log_file)

    config_files = glob.glob(f"{config_dir}/*.ini")

    print(f"{Fore.GREEN}Running simulations for {config_files} {Style.RESET_ALL}")

    i = 0
    for config_file in config_files:
        i += 1
        print(f"{Fore.GREEN}\nRunning for config file {config_file} {Fore.YELLOW} ({i}/{len(config_files)}) "
              f"{Style.RESET_ALL}")
        configs_out_dir = f"{out_dir}/{Path(config_file).stem}"

        if os.system(f"{dir_path}{MAIN} -t {topology} -l {odl_log_file} -o {configs_out_dir} -c {config_file} -nr"
                     f" --no-raw"):
            exit(1)

    print(f"{Fore.GREEN} Finished {len(config_files)} simulations {Style.RESET_ALL}")
    print_time_elapsed(start_time, "took")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--topology", "-t", help="target network topology", metavar="FILE", required=True)
    parser.add_argument("--odl_log", "-l", help="odl log", metavar="FILE", required=True)
    parser.add_argument("--out", "-o", help="output directory", metavar="DIR", required=True)
    parser.add_argument("--config", "-c", help="directory containing config files", metavar="DIR", required=True)

    args = parser.parse_args()
    main(args.topology, args.odl_log, args.out, args.config)
