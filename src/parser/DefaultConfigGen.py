import configparser


def gen_default_config(out_file: str, in_file=None):
    # print("generating default config.ini")
    config_file = open(out_file, "w+")
    config_file.write("")

    config = configparser.ConfigParser()

    if in_file:
        config.read(in_file)
        if "Parser" in config:
            del config["Parser"]

    config.write(config_file)
    config_file.close()
