import os
import time
import re
import json


def print_time_elapsed(start_time: float, title: str):
    elapsed_time = time.time() - start_time
    elapsed_time *= 1000
    elapsed_time = round(elapsed_time, 3)
    print("%s %sms" % (title, elapsed_time))


def create_dir(path: str):
    if not os.path.isdir(path):
        os.makedirs(path)


def ensure_in(target: dict, keys: list):
    if not keys:
        return True
    elem = keys.pop(0)
    if elem in target:
        return ensure_in(target[elem], keys)
    else:
        return False


def to_plain_binary(number):
    binary = bin(number)
    binary_re = re.match("0b(\\d+)", binary)
    return binary_re.group(1).zfill(8)


def to_vid(number):
    # since vid is 12 bit wide and 0x000 and 0xFFF are reserved
    return int(number) % 0xFFE + 1


def get_from_dict(in_dict, path: list):
    if len(path) == 0:
        return in_dict
    else:
        val = path.pop(0)
        return get_from_dict(in_dict[val], path)


def get_data_from_request(req: dict, path: list):
    if ensure_in(req, ["request", "body"] + path):
        return get_from_dict(req["request"]["body"], path)
    elif ensure_in(req, ["request", "body", "content"] + path):
        return get_from_dict(req["request"]["body"]["content"], path)
    elif ensure_in(req, ["response", "body"] + path):
        return get_from_dict(req["response"]["body"], path)
    elif ensure_in(req, ["response", "body", "content"] + path):
        return get_from_dict(req["response"]["body"]["content"], path)
    else:
        return {}


def write_dict_as_json(data: dict, out_file_path: str):
    tmp = open(out_file_path, "w+")
    tmp.write("")
    tmp.close()

    target_file = open(out_file_path, "a")
    result_json = json.dumps(data, indent=2)
    target_file.write(result_json)
