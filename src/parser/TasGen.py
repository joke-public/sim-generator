import os
import re
import json

import Util


def tas_fragment_from_response(interfaces: dict):
    if interfaces == {}:
        return {}
    result = []
    for interface in interfaces:
        port = re.match("\\w+(\\d+)", interface["name"]).group(1)
        parameters = interface["ieee802-dot1q-bridge:bridge-port"]["ieee802-dot1q-sched:gate-parameters"]
        gate_enabled = parameters["gate-enabled"]
        if not bool(gate_enabled):
            result.append({"port": port, "entries": [{"gate-status": "11111111"}]})
        else:
            default_bitvektor = Util.to_plain_binary(parameters["admin-gate-states"])
            list_len = int(parameters["admin-control-list-length"])
            if list_len > 0:
                control_list = parameters["admin-control-list"]
                port_tas = []
                for entry in control_list:
                    gate_val = Util.to_plain_binary(entry["sgs-params"]["gate-states-value"])
                    length = int(entry["sgs-params"]["time-interval-value"] / 1000)
                    port_tas.append({"gate-status": gate_val, "length": length})
                result.append({"port": port, "entries": port_tas})
            else:
                result.append({"port": port, "entries": [{"gate-status": default_bitvektor}]})
    return result


def only_latest_switch_configs(all_entries: dict):
    tmp = {}
    for switch_entry in all_entries:
        tmp[switch_entry["switch"]] = switch_entry
    latest_entries = []
    for switch_entry in tmp:
        latest_entries.append(tmp[switch_entry])
    return latest_entries
