import json
import configparser
import re
import Util


class OffsetPostprocessor(object):
    def __init__(self, config_file: str):
        self.config = configparser.ConfigParser()
        self.config.read(config_file)
        self.flow_offsets = {}
        self.switch_offsets = {}

        flow_re = re.compile("^flow-(\\S+)-offset$")
        switch_re = re.compile("^switch-(\\S+)-port-(\\S+)-offset$")
        if "Parser" in self.config:
            for entry in self.config["Parser"]:
                match = flow_re.match(entry)
                if match:
                    target = match.group(1)
                    offset = self.config["Parser"][entry]
                    if target == "*":
                        self.flow_offsets["*"] = offset
                    else:
                        vid = Util.to_vid(int(target))
                        self.flow_offsets[vid] = offset

                match = switch_re.match(entry)
                if match:
                    target_switch = match.group(1)
                    target_port = match.group(2)
                    offset = self.config["Parser"][entry]
                    if target_switch not in self.switch_offsets:
                        self.switch_offsets[target_switch] = {}
                    self.switch_offsets[target_switch][target_port] = offset

    def add_offset_to_flows(self, flows_file: str):
        if len(self.flow_offsets) == 0:
            return

        file = open(flows_file, "r")
        contents = file.read()
        data_store = json.loads(contents)

        for flow in data_store:
            start = int(flow["start"])
            repeat = int(flow["repeat"])
            vid = flow["vid"]
            offset = 0
            if vid in self.flow_offsets:
                offset = self.flow_offsets[vid]
            elif "*" in self.flow_offsets:
                offset = self.flow_offsets["*"]

            if offset != 0:
                new_start = (start + int(offset)) % repeat
                flow["start"] = new_start

        Util.write_dict_as_json(data_store, flows_file)

    def add_offset_to_switches(self, tas_file: str):
        if len(self.switch_offsets) == 0:
            return

        file = open(tas_file, "r")
        contents = file.read()
        data_store = json.loads(contents)

        for entry in data_store:
            switch = entry["switch"]
            for sub_entry in entry["controls"]:
                port = sub_entry["port"]
                if len(sub_entry["entries"]) > 1:
                    offset = 0
                    if switch in self.switch_offsets and port in self.switch_offsets[switch]:
                        offset = int(self.switch_offsets[switch][port])
                    elif switch in self.switch_offsets and "*" in self.switch_offsets[switch]:
                        offset = int(self.switch_offsets[switch]["*"])
                    elif "*" in self.switch_offsets and port in self.switch_offsets["*"]:
                        offset = int(self.switch_offsets["*"][port])
                    elif "*" in self.switch_offsets and "*" in self.switch_offsets["*"]:
                        offset = int(self.switch_offsets["*"]["*"])

                    if offset != 0:
                        old_entries = sub_entry["entries"]
                        new_entries = []
                        remaining_offset = offset
                        while remaining_offset > 0:
                            tail = old_entries.pop(-1)
                            if int(tail["length"]) == remaining_offset:
                                new_entries.insert(0, tail)
                                remaining_offset = 0
                            elif int(tail["length"]) > remaining_offset:
                                tail["length"] = int(tail["length"] - remaining_offset)
                                new_entries.insert(0, {"gate-status": tail["gate-status"], "length": remaining_offset})
                                old_entries.append(tail)
                                remaining_offset = 0
                            elif int(tail["length"]) < remaining_offset:
                                remaining_offset -= int(tail["length"])
                                new_entries.insert(0, tail)

                        for old_entry in old_entries:
                            new_entries.append(old_entry)
                        sub_entry["entries"] = new_entries

        Util.write_dict_as_json(data_store, tas_file)
