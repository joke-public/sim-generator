import json
import re


def build_network(topology: dict, mac_host: dict):
    # print("Generating topology file")
    result = {"switches": {}, "hosts": {}, "links": []}

    host_id_host = {}

    nodes = topology["node"]

    for node in nodes:
        node_id = node["node-id"]
        z = re.match("^openflow:(\\d+)$", node_id)
        if z:
            switch_name = "s%s" % z.group(1)
            count = 0
            for link in node["termination-point"]:
                link_re = re.match("^openflow:\\d+:\\d+$", link["tp-id"])
                if link_re:
                    count += 1

            result["switches"].update({switch_name: {"num_ports": count}})

        else:
            z = re.match("^host:", node_id)
            if z:
                mac = node["host-tracker-service:id"]
                host = mac_host[mac]
                result["hosts"].update({host: {"mac": mac}})
                host_id_host[node_id] = host

    for link in topology["link"]:
        a = re.match("^openflow:(\\d+):(\\d+)$", link["source"]["source-tp"])
        a_result = {}
        if a:
            port = int(a.group(2)) - 1
            a_result = {"node": "s%s" % a.group(1), "port": port}
        else:
            a = re.match("^host:", link["source"]["source-tp"])
            if a:
                a_result = {"node": host_id_host[link["source"]["source-tp"]]}

        b = re.match("^openflow:(\\d+):(\\d+)$", link["destination"]["dest-tp"])
        b_result = {}
        if b:
            port = int(b.group(2)) - 1  # since port 0 is localhost in openflow, but in nesting it is the first port...
            b_result = {"node": "s%s" % b.group(1), "port": port}
        else:
            b = re.match("^host:", link["destination"]["dest-tp"])
            if b:
                b_result = {"node": host_id_host[link["destination"]["dest-tp"]]}

        result["links"].append({"A": a_result, "B": b_result})
        result["links"] = filter_duplicates(result["links"])

    return result


def filter_duplicates(in_list: list):
    entry_map = {}
    result = []

    for entry in in_list:
        link_id = build_identifier(entry, False)
        link_id_reversed = build_identifier(entry, True)
        if link_id in entry_map or link_id_reversed in entry_map:
            continue
        result.append(entry)
        entry_map.update({link_id: True})

    return result


def build_identifier(link: dict, reverse: bool):
    if "port" in link["A"]:
        a_res = "%s-%s" % (link["A"]["node"], link["A"]["port"])
    else:
        a_res = link["A"]["node"]

    if "port" in link["B"]:
        b_res = "%s-%s" % (link["B"]["node"], link["B"]["port"])
    else:
        b_res = link["B"]["node"]

    if reverse:
        return "%s %s" % (a_res, b_res)
    else:
        return "%s %s" % (b_res, a_res)
