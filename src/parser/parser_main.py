#!/usr/bin/env python3

import argparse
import time
import os
import json

import FlowGen
import DefaultConfigGen
import Util
import OdlLogParser
import OffsetPostprocessor


def main(topology: str, odl_log: str, out_dir: str, config=None):
    start_time = time.time()
    if not os.path.isdir(out_dir):
        Util.create_dir(out_dir)

    file = open(topology, "r")
    contents = file.read()
    data_store = json.loads(contents)

    mac_host = {}

    for host in data_store["topology"]["hosts"]:
        mac = data_store["topology"]["hosts"][host]
        mac_host[mac] = host

    vid_flow_dict = FlowGen.build_flows(topology)
    OdlLogParser.parse_odl_log(odl_log, vid_flow_dict, data_store["topology"]["hosts"], mac_host, out_dir)
    DefaultConfigGen.gen_default_config("%s/config.ini" % out_dir, in_file=config)

    if config:
        postProcessor = OffsetPostprocessor.OffsetPostprocessor(config)
        postProcessor.add_offset_to_flows("%s/flows.json" % out_dir)
        postProcessor.add_offset_to_switches("%s/tas.json" % out_dir)

    Util.print_time_elapsed(start_time, "Parsing the odl_log and the topology took")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--topology", "-t", help="target network topology", metavar="FILE", required=True)
    parser.add_argument("--odl_log", "-l", help="odl log containing tas rules", metavar="FILE", required=True)
    parser.add_argument("--config", "-c", help="config to overwrite defaults", metavar="FILE", required=False)
    parser.add_argument("--out", "-o", help="output directory", metavar="DIR", required=True)
    args = parser.parse_args()

    main(args.topology, args.odl_log, args.out, config=args.config)
