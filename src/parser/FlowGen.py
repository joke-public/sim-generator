import json

import Util


def build_flows(in_file: str):
    file = open(in_file, "r")
    contents = file.read()
    data_store = json.loads(contents)

    result = []

    stream_dict = {}

    streams = data_store["streams"]

    for stream in streams:
        vid = Util.to_vid(stream["dest_port"])
        sender = stream["sender"]
        receiver = stream["receivers"][0]
        # start = stream["traffic"]["time_offset"]
        # repeat = stream["traffic"]["time_interarrival"]
        # size = stream["traffic"]["framesize"]
        entry = {"sender": sender, "receiver": receiver, "vid": vid}
        flow_name = stream["name"]
        stream_dict.update({flow_name: entry})
        result.append(entry)

    return stream_dict
