import json
import re
from colorama import Fore
from colorama import Style

import Util
import NetworkGen
import TasGen


def parse_odl_log(in_file: str, stream_dict: dict, host_mac: dict, mac_host: dict, out_path: str):
    # print("Parsing odl log")

    file = open(in_file, "r")
    contents = file.read()
    data_store = json.loads(contents)
    network = {}
    tci_map = {}
    routing = {}
    tas = []

    for request in data_store:
        if "request" in request:
            path_regex = re.match("\\S+/openflow:(\\d+)/\\S+", request["request"]["path"])
            if path_regex:
                switch = "s%s" % path_regex.group(1)
                # print(switch)

                flow = Util.get_data_from_request(request, ["flow"])
                if not flow == {}:
                    parse_flow(flow, stream_dict, host_mac, tci_map, routing)

                interfaces = Util.get_data_from_request(request, ["interfaces", "interface"])
                if not interfaces == {}:
                    tas_fragment = TasGen.tas_fragment_from_response(interfaces)
                    if not tas_fragment == {}:
                        tas.append({"switch": switch, "controls": tas_fragment})
                # print(interfaces)

            path_regex = re.match("\\S+/network-topology:network-topology/topology/flow:\\d+",
                                  request["request"]["path"])
            if path_regex:
                topology = Util.get_data_from_request(request, ["topology"])
                if topology:
                    network = NetworkGen.build_network(topology[0], mac_host)
        elif "talker" and "status" in request:
            if request["status"]:
                offset = request["status"]["interface_configuration"]["_timeawareoffset"]
                if offset:
                    parse_talker_spec(request["talker"], offset, stream_dict)
                else:
                    parse_talker_spec(request["talker"], '0', stream_dict)

    Util.write_dict_as_json(tci_map, "%s/vid-map.json" % out_path)
    Util.write_dict_as_json(routing, "%s/routing.json" % out_path)
    Util.write_dict_as_json(network, "%s/network.json" % out_path)
    latest_tas_entries = TasGen.only_latest_switch_configs(tas)
    Util.write_dict_as_json(latest_tas_entries, "%s/tas.json" % out_path)
    flows = list(stream_dict.values())
    Util.write_dict_as_json(flows, "%s/flows.json" % out_path)


def parse_talker_spec(data: dict, offset: str, stream_dict: dict):
    if data and "traffic_specification" in data:
        spec = data["traffic_specification"]
        dest_port = int(data["data_frame_specification"]["destination_port"])

        vid = Util.to_vid(dest_port)
        repeat = int(int(spec["interval"]["numerator"]) / int(spec["interval"]["denominator"]))
        # earliest_transmit_offset = int(spec["earliest_transmit_offset"])
        # latest_transmit_offset = int(spec["latest_transmit_offset"])
        max_frame_size = int(spec["max_frame_size"])

        max_frames_per_interval = int(spec["max_frames_per_interval"])
        assert (max_frames_per_interval == 1), f"{Fore.RED}Warning, log specifies multiple packages per burst for dest " \
            f"port {dest_port}, this is not supported yet for the simulation. Only one package will be send per " \
            f"interval {Style.RESET_ALL}"

        transmission_selection = int(spec["transmission_selection:"])
        assert (transmission_selection == 0), f"{Fore.RED}Warning, transmission_selection was set for stream with dest " \
            f"port {dest_port}, this will be ignored by the simulation generator! {Style.RESET_ALL}"

        entry = stream_dict[data["name"]]
        assert (entry["vid"] == vid), "stream with same name got different destination ports"

        entry["start"] = int(int(offset) / 1000)
        entry["size"] = max_frame_size
        entry["repeat"] = int(repeat / 1000)


def parse_flow(flow: dict, vid_flow_dict: dict, host_mac: dict, out_tci_map: dict, out_routing: dict):
    if Util.ensure_in(flow, ["match", "udp-destination-port"]):
        vid = Util.to_vid(flow["match"]["udp-destination-port"])
    else:
        vid = 0  # 0 means that this rule matches to all vid s, that a re not explicitly set

    queue = -1
    switch = "err"
    port = -1
    for action in flow["instructions"]["instruction"][0]["apply-actions"]["action"]:
        if "set-queue-action" in action:
            queue = action["set-queue-action"]["queue-id"]
        elif "output-action" in action:
            string = action["output-action"]["output-node-connector"]
            conn_re = re.match("openflow:(\\d+):(\\d+)", string)
            switch = "s%s" % conn_re.group(1)
            port = int(conn_re.group(2)) - 1

    if switch not in out_tci_map:
        out_tci_map[switch] = []
    out_tci_map[switch].append({"queue": queue, "port": port, "vid": vid})

    flow_id = flow["id"]
    short_flow_id = re.match("rtman__(\\S+)__openflow:\\d+", flow_id)
    if short_flow_id:
        flow = vid_flow_dict[short_flow_id.group(1)]
        if switch not in out_routing:
            out_routing[switch] = []
        out_routing[switch].append({"mac": host_mac[flow["receiver"]], "vid": flow["vid"], "port": port})
