import json
import xml.etree.ElementTree as ET

import Util


def build_tas_xml(in_file: str, out_file: str):
    file = open(in_file, "r")
    contents = file.read()
    data_store = json.loads(contents)

    data = ET.Element('schedule')
    default_cycle = ET.SubElement(data, "defaultcycle")
    default_cycle.text = "1000us"

    for switch_entry in data_store:
        cycle = 0
        for port_entry in switch_entry["controls"]:
            if len(port_entry["entries"]) > 1:
                sum_length = 0
                for entry in port_entry["entries"]:
                    sum_length += int(entry["length"])

                if cycle == 0:
                    cycle = sum_length
                else:
                    cycle = Util.lcm(cycle, sum_length)
        if cycle == 0:
            cycle = 1000

        switch_xml_entry = ET.SubElement(data, "switch")
        switch = switch_entry["switch"]
        switch_xml_entry.set("name", switch)
        cycle_xml_entry = ET.SubElement(switch_xml_entry, "cycle")
        cycle_xml_entry.text = "%dus" % cycle

        for port_entry in switch_entry["controls"]:
            port = port_entry["port"]
            port_xml_entry = ET.SubElement(switch_xml_entry, "port")
            port_xml_entry.set("id", "%s" % port)
            if len(port_entry["entries"]) > 1:
                for entry in port_entry["entries"]:
                    entry_xml = ET.SubElement(port_xml_entry, "entry")
                    length_xml = ET.SubElement(entry_xml, "length")
                    length_xml.text = "%sus" % entry["length"]
                    bitvektor_xml = ET.SubElement(entry_xml, "bitvector")
                    bitvektor_xml.text = entry["gate-status"]
            else:
                for entry in port_entry["entries"]:
                    entry_xml = ET.SubElement(port_xml_entry, "entry")
                    length_xml = ET.SubElement(entry_xml, "length")
                    length_xml.text = "%sus" % cycle
                    bitvektor_xml = ET.SubElement(entry_xml, "bitvector")
                    bitvektor_xml.text = entry["gate-status"]

    result_string = ET.tostring(data, encoding="unicode")
    result_file = open(out_file, "w")
    result_file.write(result_string)
    result_file.close()
