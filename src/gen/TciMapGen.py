import json
import xml.etree.ElementTree as ET


def parse(in_file: str, out_path: str):
    file = open(in_file, "r")
    contents = file.read()
    data_store = json.loads(contents)

    tmp = {}

    for switch in data_store:
        if switch not in tmp:
            tmp[switch] = {}
        switch_entries = data_store[switch]
        for entry in switch_entries:

            port = entry["port"]
            if port not in tmp[switch]:
                tmp[switch][port] = []
            queue = entry["queue"]
            vid = entry["vid"]
            tmp[switch][port].append({"queue": queue, "vid": vid})

    for switch in tmp:
        for port in tmp[switch]:
            mappings_xml = ET.Element('mappings')
            default_xml = ET.SubElement(mappings_xml, "default")
            default_xml.text = "0"
            for entry in tmp[switch][port]:
                mapping_xml = ET.SubElement(mappings_xml, "mapping")
                queue = entry["queue"]
                vid = entry["vid"]
                queue_xml = ET.SubElement(mapping_xml, "queue")
                queue_xml.text = "%s" % queue
                tci_xml = ET.SubElement(mapping_xml, "tci")
                tci_xml.text = "%s" % vid

            file_name = "tci-map-%s-%s.xml" % (switch, port)

            result_string = ET.tostring(mappings_xml, encoding="unicode")
            result_file = open("%s/%s" % (out_path, file_name), "w")
            result_file.write(result_string)
            result_file.close()
