import configparser


def gen_default_config(in_file=None):
    config = configparser.ConfigParser()
    config.add_section("General")
    config["General"]["scenario_name"] = "GeneratedScenario"
    config["General"]["result-dir"] = "results"
    config["General"]["package"] = "tsn_sim.simulations.generated"

    config.add_section("Debug")
    config["Debug"]["display-Addresses"] = "false"
    config["Debug"]["verbose"] = "false"
    config["Debug"]["promiscuous"] = "true"
    config["Debug"]["register-protocol"] = "true"

    config.add_section("Sim")
    config["Sim"]["sim-time-limit"] = "0.1s"
    config["Sim"]["record-eventlog"] = "false"
    config["Sim"]["check-signals"] = "false"
    config["Sim"]["debug-on-errors"] = "false"
    config["Sim"]["result-recording-modes"] = "default"
    config["Sim"]["clock-rate"] = "1us"
    config["Sim"]["# *-clock-typename"] = "FaultyClock"
    config["Sim"]["# *-clock-offset"] = "10us"
    config["Sim"]["# *-clock-jitter"] = "2us"
    config["Sim"]["talker-*-jitter"] = "0.0"  # in s

    config.add_section("Switches")
    config["Switches"]["*-eth-*-processing-delay"] = "5us"
    config["Switches"]["gate-controller-hold-and-release"] = "false"
    config["Switches"]["*-eth-*-queue-*-ts-algorithm"] = "StrictPriority"
    config["Switches"]["*-eth-*-queue-*-express"] = "true"
    config["Switches"]["*-eth-*-queue-*-buffer-capacity"] = "365280b"
    config["Switches"]["*-eth-*-frame-preemption"] = "false"

    config.add_section("Topology")
    config["Topology"]["link_delay"] = "0.1us"
    config["Topology"]["link_data_rate"] = "1Gbps"

    if in_file:
        config.read(in_file)

    return config
