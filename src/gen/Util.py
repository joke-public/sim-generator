import os
import time
from math import gcd


def print_time_elapsed(in_time: float, title: str):
    elapsed_time = time.time() - in_time
    elapsed_time = round(elapsed_time, 3)
    print("%s %ss" % (title, elapsed_time))


def create_dir(path: str):
    if not os.path.isdir(path):
        os.makedirs(path)


def lcm(a: int, b: int):
    return abs(a*b) // gcd(a, b)


def lcm_list(numbers: list):
    a = numbers[0]
    # works because lcm(a, a) = a
    for number in numbers:
        a = lcm(a, number)
    return a


def repeat(start: int, addition: int, max_val: int):
    counter = start
    result = []
    while counter < max_val:
        result.append(counter)
        counter += addition
    return result
