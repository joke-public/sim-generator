#!/usr/bin/env python3

import argparse
import time
import os
import shutil
import IniParser
import Topology
import Util
import RoutingParser
import TrafficGen
import TasGen
import TciMapGen
import DefaultConfigGen

start_time = time.time()

parser = argparse.ArgumentParser()
parser.add_argument("--config", "-c", help="config file for sim generation", metavar="FILE", required=True)
parser.add_argument("--topology", "-t", help="target network topology", metavar="FILE", required=True)
parser.add_argument("--out", "-o", help="output directory", metavar="DIR", required=True)
parser.add_argument("--timeAwareShaper", "-tas", help="time aware shaper configuration xml file", metavar="FILE",
                    required=True)
parser.add_argument("--routing", "-r", help="routing json", metavar="FILE", required=True)
parser.add_argument("--flows", help="flow definition", metavar="FILE", required=True)
parser.add_argument("--vid_map", help="tci to queue mapping", metavar="FILE", required=True)
parser.add_argument("--package", help="package of the network.ned", required=False)

args = parser.parse_args()

if os.path.isdir(args.out):
    shutil.rmtree(args.out)
Util.create_dir("%s/xml/tci-maps" % args.out)
# delete all files in target dir
# copyfile(args.timeAwareShaper, "%s/xml/tas.xml" % args.out)
TasGen.build_tas_xml(args.timeAwareShaper, "%s/xml/tas.xml" % args.out)

config = DefaultConfigGen.gen_default_config(in_file=args.config)

if args.package:
    config["General"]["package"] = args.package

TciMapGen.parse(args.vid_map, "%s/xml/tci-maps" % args.out)
Topology.parse_topology(args.topology, "%s/network.ned" % args.out, config)
IniParser.build_ini(args.topology, "%s/xml/tci-maps" % args.out, "%s/sim.ini" % args.out, config)
RoutingParser.parse_routing(args.routing, "%s/xml/routing.xml" % args.out)
TrafficGen.parse(args.topology, args.flows, "%s/xml/traf-gen.xml" % args.out)

Util.print_time_elapsed(start_time, "Generating the nesting simulation took")
