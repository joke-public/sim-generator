from colorama import Fore
from colorama import Style


def validate(data: dict):
    validation_errs = validate_hosts(data["hosts"])
    validation_errs.extend(validate_streams(data["streams"]))
    return validation_errs


def validate_hosts(hosts: dict):
    validation_errs = []
    for host in hosts:
        if "received_periodically" in hosts[host]:
            is_periodic = hosts[host]["received_periodically"]["is_periodic"]
            if not is_periodic:
                validation_errs.append("Host %s: No periodicity found in the streams received" % host)
    return validation_errs


def validate_streams(streams: dict):
    validation_errs = []
    for stream in streams:
        if "max_allowed_latency" in streams[stream]:
            max_allowed_latency = float(streams[stream]["max_allowed_latency"]) / 1000000000  # since it is in ns
            max_latency = float(streams[stream]["latency_stats"]["max"])
            stream_name = streams[stream]["name"]
            if max_latency > max_allowed_latency:
                validation_errs.append("Stream %s (-> vid %s): max latency (%s) higher than allowed (%s)"
                                       % (stream_name, stream, max_latency, max_allowed_latency))
    return validation_errs
