import Util


def parse_switch(data: dict, skip_empty: bool):
    name = data[3]
    values_string_list = [float(x) for x in ("%s" % data["vecvalue"]).split(" ")]
    timing_string_list = [float(x) for x in ("%s" % data["vectime"]).split(" ")]
    if skip_empty and len(values_string_list) < 2:
        return {}, {}, {}, False
    if name == "queueLength:vector":
        values = [int(x) for x in values_string_list]
        return {"queueLength": Util.calc_stats(values)}, values_string_list, timing_string_list, False
    elif name == "queueingTime:vector":
        values = [float(x) for x in values_string_list]
        return {"queueingTime": Util.calc_stats(values)}, values_string_list, timing_string_list, True

    return {}, {}, {}, False
