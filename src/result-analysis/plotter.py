#!/usr/bin/env python3

import argparse
import json

import matplotlib.pyplot as plt
import numpy as np


def plot_dicts(x_axis: dict, y_axis: dict, xlabel="time in s", ylabel="data value", title="data of %s"):
    num_streams = len(x_axis)
    fig, axs = plt.subplots(num_streams, 1)
    fig.subplots_adjust(hspace=1.2)

    i = 0
    for identifier in x_axis:
        axs[i].plot(x_axis[identifier], y_axis[identifier])
        axs[i].set(xlabel=xlabel, ylabel=ylabel, title=title % identifier)
        axs[i].grid()
        i += 1

    plt.show()


def plot_hist(y_axis: dict, xlabel="Time in s", ylabel="Data value", title="Data of %s", in_bins=10):
    num_streams = len(y_axis)

    i = 0
    fig, axs = plt.subplots(num_streams, 1)
    fig.subplots_adjust(hspace=1.2)
    for identifier in y_axis:
        hist, bins = np.histogram(y_axis[identifier], bins=in_bins)
        width = 0.9 * (bins[1] - bins[0])
        center = (bins[:-1] + bins[1:]) / 2
        axs[i].bar(center, hist, align='center', width=width)
        axs[i].set(xlabel=xlabel, ylabel=ylabel, title=title % identifier)
        axs[i].grid()
        i += 1

    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", "-i", help="input result_raw.json", metavar="FILE", required=True)
    parser.add_argument("--queue_length", "-ql", help="plot queue length of all switches", action='store_true')
    parser.add_argument("--queuing_time", "-qt", help="plot queueing time of all switches", action='store_true')
    parser.add_argument("--delay", "-d", help="plot end to end delay of all flows", action='store_true')

    parser.add_argument("--delay_histogram", "-dh", help="plot end to end delay histogram", action='store_true')
    parser.add_argument("--queue_length_histogram", "-qlh", help="plot queue length histogram",
                        action='store_true')
    parser.add_argument("--queuing_time_histogram", "-qth", help="plot queueing time histogram",
                        action='store_true')

    args = parser.parse_args()

    file = open(args.input, "r")
    contents = file.read()
    data_store = json.loads(contents)

    if args.queuing_time:
        plot_dicts(data_store["switches"]["queuing_time"]["timings"],
                   data_store["switches"]["queuing_time"]["queuing_time"],
                   ylabel="Queueing time in s", title="Queueing time of %s")

    if args.queue_length:
        plot_dicts(data_store["switches"]["queue_length"]["timings"],
                   data_store["switches"]["queue_length"]["queue_length"],
                   ylabel="Queue length", title="Queue length of %s")

    if args.delay:
        plot_dicts(data_store["flows"]["timings"],
                   data_store["flows"]["delays"],
                   ylabel="End to end delay", title="End to end delay of %s")

    if args.delay_histogram:
        plot_hist(data_store["flows"]["delays"], xlabel="Delay in s", ylabel="Number of packages",
                  title="Delay histogram for %s", in_bins=25)

    if args.queue_length_histogram:
        plot_hist(data_store["switches"]["queue_length"]["queue_length"], xlabel="Queue length",
                  ylabel="Number of queue changes",
                  title="Queue length histogram for %s")

    if args.queuing_time_histogram:
        plot_hist(data_store["switches"]["queuing_time"]["queuing_time"], xlabel="Queuing time in s",
                  ylabel="Number of packages",
                  title="Queuing time histogram for %s",
                  in_bins=25)
