import json
import Util


def generate_additional_info(topology_file: str):
    file = open(topology_file, "r")
    contents = file.read()
    data_store = json.loads(contents)
    streams = data_store["streams"]
    result = {}
    for stream in streams:
        udp_dest_port = stream["dest_port"]
        name = stream["name"]
        max_latency = stream["max_latency"]
        talker = stream["sender"]
        listener = stream["receivers"][0]
        vid = Util.to_vid(int(udp_dest_port))
        result[vid] = {"udp_dest_port": udp_dest_port, "name": name, "max_allowed_latency": max_latency,
                       "talker": talker, "listener": listener}
    return result
