def find_periods(in_data: [], max_per_length: int):
    data = in_data
    is_periodic = True
    period = []
    period_length = 0
    m_per_length = min(int(len(data) / 2) + 1, max_per_length)
    for x in range(1, m_per_length):
        period = data[:x]
        period_length = len(period)
        is_periodic = True
        for idx in range(0, len(data)):
            if not period[idx % period_length] == data[idx]:
                is_periodic = False
                break
        if is_periodic:
            break

    result = {"is_periodic": is_periodic, "max-period-length": max_per_length}
    if is_periodic:
        result.update({"period": period, "period_length": period_length})
    return result
