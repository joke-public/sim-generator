#!/usr/bin/env python3

import argparse
import math
import os
import re
from shutil import which

import pandas
from colorama import Fore
from colorama import Style

import Periodic
import SwitchResultParser
import TopologyAddon
import Util
import Validator

parser = argparse.ArgumentParser()
parser.add_argument("--input", "-i", help="input .csv or .vec file", metavar="FILE", required=True)
parser.add_argument("--topology", "-t", help="input topology to extract original stream names from", metavar="FILE",
                    required=False)
parser.add_argument("--max-per-length", help="maximum length of the periods", default=250, type=int)
parser.add_argument("--per-skip", help="time offset for periodic analysis, defaults to 0ms", default=0, type=float)
parser.add_argument("--skip-empty", "-s", help="skip empty statistics", action='store_true')
parser.add_argument("--out", "-o", help="output directory", metavar="DIR", required=True)
parser.add_argument("--no-raw", help="doesn't save raw date", action='store_true')

args = parser.parse_args()

# print(f"{Fore.GREEN}Start parsing {os.path.abspath(args.input)} {Style.RESET_ALL}")

csv_path = args.input
remove_csv = False

if re.match("\\S+\\.vec", args.input):
    scavetool_path = which("scavetool")
    csv_path = f"{args.out}/tmp.csv"
    if scavetool_path is None:
        print(f"{Fore.RED}scavetool not found in path, likely you did not add omnetpp to your path properly."
              f"{Style.RESET_ALL}")
        exit(1)
    else:
        remove_csv = True
        print(f"Using omnetpp's scavetool to parse the .vec file to a .csv")
        if not os.path.isdir(args.out):
            os.makedirs(args.out)
        if os.system(f"scavetool x {args.input} -o {csv_path}"):
            exit(1)

print(f"Starting parsing the results")
data = pandas.read_csv(csv_path)

if remove_csv:
    os.remove(csv_path)

result = {"general": {}, "sim_parameters": {}, "streams": {}, "hosts": {}, "switches": {}}

# host_timings = {}
tmp = {}
endToEndDelay = {}
receiveTiming = {}
pk_vid = {}
vid_pk_delays = {}
vid_rcv_pk_id = {}
host_pk_recv = {}
host_vid_recv = {}

switch_queue_length = {}
switch_queue_timing = {}

switch_queueing_time = {}
switch_queueing_timing = {}

for entry in data.iterrows():
    inner = entry[1]
    idx = entry[0]
    if type(inner.module) == str and re.match("\\S+\\.(h\\d+)\\.eth.vlanEncap",
                                              inner.module) and inner.type == "vector":
        host = re.match("\\S+\\.(h\\d+)\\.eth.vlanEncap", inner.module).group(1)
        # print(inner)
        name = inner[3]
        if name == "encapPkVid:vector":
            vec_time = inner.vectime.split(" ")
            vec_value = inner.vecvalue.split(" ")
            for x in range(len(vec_time)):
                # in one of the vectors lies the vid - package id mapping:
                # vid = val & 0xFFF, packageId = val >> 12
                # I know this is super unreadable and hacky af but omnet won't let me use strings and insists on numbers
                packageId = int(vec_value[x]) >> 12
                vid = int(vec_value[x]) & 0xFFF
                if packageId not in pk_vid:
                    pk_vid[packageId] = vid
        else:
            module = inner.module
            vec_time = inner.vectime.split(" ")
            vec_value = inner.vecvalue.split(" ")
            for x in range(len(vec_time)):
                if vec_value[x] in tmp:
                    endToEndDelay[vec_value[x]] = math.fabs(float(tmp[vec_value[x]]) - float(vec_time[x]))
                    receiveTiming[vec_value[x]] = max(float(vec_time[x]), float(tmp[vec_value[x]]))
                else:
                    tmp[vec_value[x]] = float(vec_time[x])
            if name == "decapPk:vector":
                if host not in host_pk_recv:
                    start_idx = 0
                    for sas in range(len(vec_time)):
                        if float(vec_time[sas]) >= args.per_skip:
                            start_idx = sas
                            break
                    host_pk_recv[host] = [int(x) for x in vec_value[start_idx:]]

    elif type(inner.module) == str and inner.type == "vector":
        switch_re = re.match("\\S+\\.(s\\d+)\\.eth\\[(\\d+)\\]\\.queuing\\.queues\\[(\\d+)\\]", inner.module)
        if switch_re:
            switch = switch_re.group(1)
            port = "port_%s" % switch_re.group(2)
            queue_idx = "queue_%s" % switch_re.group(3)
            switch_entry, switch_val, switch_timing, is_time = SwitchResultParser.parse_switch(inner, args.skip_empty)
            if switch_entry == {}:
                continue
            if switch not in result["switches"]:
                result["switches"][switch] = {}
            if port not in result["switches"][switch]:
                result["switches"][switch][port] = {}
            if queue_idx not in result["switches"][switch][port]:
                result["switches"][switch][port][queue_idx] = {}
            result["switches"][switch][port][queue_idx].update(switch_entry)

            if is_time:
                switch_queueing_time[f"switch_{switch}_{port}"] = switch_val
                switch_queueing_timing[f"switch_{switch}_{port}"] = switch_timing
            else:
                switch_queue_length[f"switch_{switch}_{port}"] = switch_val
                switch_queue_timing[f"switch_{switch}_{port}"] = switch_timing

    elif inner.type == "param":
        attr_name = inner["attrname"]
        attr_val = inner["attrvalue"]
        result["sim_parameters"][attr_name] = attr_val

for host in host_pk_recv:
    host_vid_recv[host] = []
    for package in host_pk_recv[host]:
        host_vid_recv[host].append(pk_vid[int(package)])
    if host not in result["hosts"]:
        result["hosts"][host] = {}
    per = Periodic.find_periods(host_vid_recv[host], args.max_per_length)
    per.update({"skipped": "first %ss" % args.per_skip})
    result["hosts"][host].update({"received_periodically": per})

vid_pk_recv = {}

for value in receiveTiming:
    vid = pk_vid[int(value)]
    if vid not in vid_pk_recv:
        vid_pk_recv[vid] = []
    vid_pk_recv[vid].append(receiveTiming[value])

for value in endToEndDelay:
    vid = pk_vid[int(value)]
    if vid not in vid_pk_delays:
        vid_pk_delays[vid] = []
    vid_pk_delays[vid].append(endToEndDelay[value])

# values = [v for v in endToEndDelay.values()]

for vid in vid_pk_delays:
    if vid not in result["streams"]:
        result["streams"][vid] = {}
    stats = Util.calc_stats(vid_pk_delays[vid])
    stats.update({"packages_received": len(vid_pk_delays[vid])})
    result["streams"][vid].update({"latency_stats": stats})

raw_result = {"flows": {"timings": vid_pk_recv, "delays": vid_pk_delays}, "switches": {}}
raw_result["switches"]["queuing_time"] = {"timings": switch_queueing_timing, "queuing_time": switch_queueing_time}
raw_result["switches"]["queue_length"] = {"timings": switch_queue_timing, "queue_length": switch_queue_length}

if args.topology and args.topology != "None":
    additional_info = TopologyAddon.generate_additional_info(args.topology)
    raw_result["flows"].update({"info": additional_info})
    for vid in additional_info:
        if vid in result["streams"]:
            result["streams"][vid].update(additional_info[vid])
        else:
            print(f"{Fore.YELLOW}Warning: generated additional info for dest %s (-> vid %s), which is not in the sim "
                  f"results! " % (additional_info[vid]["udp_dest_port"], vid))
            print(
                f"|- This can mean, that the provided topology file is not the one, that was used to create "
                f"the odl_log {Style.RESET_ALL}")

validation_errors = Validator.validate(result)

result["general"]["validation"] = {}
result["general"]["validation"].update({"valid": not validation_errors, "errors": validation_errors})

if validation_errors:
    print(f"{Fore.RED}Found validation errors:")
    for err in validation_errors:
        print("|- %s" % err)
    print(f"{Style.RESET_ALL}")
else:
    print(f"{Fore.GREEN}no validation errors found, this means the config satisfies its real-time constraints"
          f"{Style.RESET_ALL}")

print(f"{Fore.GREEN}Creating result at %s{Style.RESET_ALL}" % os.path.abspath("%s/result.json" % args.out))

Util.create_dir(args.out)
Util.write_dict_as_json(result, "%s/result.json" % args.out)
if not args.no_raw:
    Util.write_dict_as_json(raw_result, "%s/result_raw.json" % args.out, intend=None)  # to save disk space

print(f"{Fore.GREEN}Finished!{Style.RESET_ALL}")
