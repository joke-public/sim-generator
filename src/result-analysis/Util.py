import statistics
import json
import os


def calc_stats(values: []):
    stats = {"min": min(values), "max": max(values), "mean": statistics.mean(values),
             "median": statistics.median(values)}

    if len(values) > 1:
        stats["variance"] = statistics.variance(values)
        stats["std_deviation"] = statistics.stdev(values)

        stats["interval_size"] = stats["max"] - stats["min"]
        stats["jitter"] = max(stats["mean"] - stats["min"], stats["max"] - stats["mean"])

    return stats


def write_dict_as_json(data: dict, out_file_path: str, intend=2):
    tmp = open(out_file_path, "w+")
    tmp.write("")
    tmp.close()

    target_file = open(out_file_path, "a")
    result_json = json.dumps(data, indent=intend)
    target_file.write(result_json)


def create_dir(path: str):
    if not os.path.isdir(path):
        os.makedirs(path)


def to_vid(number):
    # since vid is 12 bit wide and 0x000 and 0xFFF are reserved
    return int(number) % 0xFFE + 1
