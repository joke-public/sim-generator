import time
import string
import random
import os
from colorama import Fore
from colorama import Style


def print_time_elapsed(in_time: float, title: str):
    elapsed_time = time.time() - in_time
    elapsed_time = round(elapsed_time, 3)
    print("%s %ss" % (title, elapsed_time))


def random_str(string_length=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(string_length))


def ensure_is_dir(path: str) -> None:
    if not os.path.isdir(path):
        print(f"{Fore.RED}{path} isn't a directory, aborting!{Style.RESET_ALL}")
        exit(1)


def ensure_is_file(path: str) -> None:
    if not os.path.isfile(path):
        print(f"{Fore.RED}{path} isn't a file, aborting!{Style.RESET_ALL}")
        exit(1)
