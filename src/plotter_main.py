#!/usr/bin/env python3
import argparse
import os
import TabCompleter
import readline
from colorama import Fore
from colorama import Style

PLOTTER_ANALYSIS_PATH = "/result-analysis/plotter.py"


def print_help() -> None:
    print(" - - - ")
    print(f"Usage:")
    print(f"plot end to end delays (d), histogram (dh)")
    print(f"plot queue length (ql), histogram (qlh)")
    print(f"plot queuing time (qt), histogram (qth)")
    print("exit (exit) (CTRL C) (CTRL D)")
    print("display this message (help)")
    print(" - - - ")


def main(input_file=None):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    # To add tab completion for selecting files
    t = TabCompleter.TabCompleter()
    readline.set_completer_delims('\t')
    readline.parse_and_bind("tab: complete")
    readline.set_completer(t.path_completer)

    print(f"{Fore.GREEN}Welcome {Fore.YELLOW}to {Fore.CYAN}the {Fore.RED}interactive{Fore.BLUE} sim data plotter !"
          f"{Style.RESET_ALL}")

    raw_result_path = ""
    if input_file:
        raw_result_path = input_file
    else:
        print(f"Specify the path to the raw results (result_raw.json):")
        raw_result_path = input()
    if not os.path.isfile(raw_result_path):
        print(f"{Fore.RED}{raw_result_path} isn't a file, aborting!{Style.RESET_ALL}")
        exit(1)

    commands = ["d", "dh", "ql", "qlh", "qt", "qth", "exit", "help"]

    t.create_list_completer(commands)
    readline.set_completer(t.list_completer)

    print_help()
    try:
        while True:
            print(f"enter command; help for list of commands")
            cmd = input().strip()
            if cmd not in commands:
                print("invalid command")
                continue
            else:
                if cmd == "exit":
                    break
                elif cmd == "help":
                    print_help()
                else:
                    if os.system(f"{dir_path}{PLOTTER_ANALYSIS_PATH} -i {raw_result_path} -{cmd}"):
                        exit(1)

    except KeyboardInterrupt:
        pass
    except EOFError:
        pass
    print("Bye")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", "-i", help="input result_raw.json", metavar="FILE")

    args = parser.parse_args()
    main(args.input)
