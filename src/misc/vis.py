#!/usr/bin/env python3


import argparse
import os

import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np

import Util


def plot_error_bars(x, y, xlabel=None, ylabel=None, title=None, scale="log", ylim=None, flags=[]):
    plt.rcParams.update({'font.size': 20})

    for data_y in y:
        mean = y[data_y]['mean']
        std_dev = y[data_y]['std_deviation']
        fmt = y[data_y]['fmt']
        plt.errorbar(x, mean, std_dev, fmt=fmt, lw=2, capsize=5, label=data_y)

    plt.yscale(scale)
    plt.legend()
    if "grid_y_minor" in flags:
        plt.grid(b=True, which='minor', axis='y')
    if "grid_x_minor" in flags:
        plt.grid(b=True, which='minor', axis='x')
    ax = plt.gca()
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    if ylim:
        ax.set_ylim(ylim)
    fig = plt.gcf()
    fig.set_size_inches(18, 7)
    plt.grid()
    plt.show()


def plot_multiple_bars(bar_names, data_y, title=None, xlabel="") -> None:
    plt.rcdefaults()
    plt.rcParams.update({'font.size': 18})
    fig, ax = plt.subplots(1, len(data_y), figsize=(16, 7), sharey='all')

    # Example data
    y_pos = np.arange(len(bar_names))

    for i in range(len(data_y)):
        offsets = data_y[i]["offset"]
        lengths = data_y[i]["length"]
        closed_legend = mpatches.Patch(color='#f2f2f2')
        open_legend = mpatches.Patch(edgecolor='black', facecolor='white', hatch="\\\\")

        legends = [closed_legend, open_legend]
        legend_desc = ['Closed', 'Open']

        color = 'black'
        if "color" in data_y[i]:
            color = data_y[i]["color"]
            change_color = next((x for x in color if x != 'black'), None)
            changed_legend = mpatches.Patch(edgecolor=change_color, facecolor='white', hatch="\\\\")
            legends.append(changed_legend)
            legend_desc.append('Open (Modified)')

        fill_left = [x + y for x, y in zip(offsets, lengths)]
        fill_left_max = max(fill_left) + 1
        fill_val = [fill_left_max - x for x in fill_left]

        ax[i].barh(y_pos, offsets, color='#f2f2f2', zorder=0)
        ax[i].barh(y_pos, lengths, left=offsets, color='none', edgecolor=color, hatch="\\\\", zorder=1, lw=2)
        ax[i].barh(y_pos, fill_val, left=fill_left, color='#f2f2f2', zorder=0)
        ax[i].set_xlabel(xlabel)
        ax[i].set_title(title[i])

        ax[i].legend(legends, legend_desc, loc='upper right')

    ax[0].set_yticks(y_pos)
    ax[0].set_yticklabels(bar_names)
    ax[0].invert_yaxis()  # labels read top-to-bottom
    fig.tight_layout()
    plt.show()


def plot_bars(bar_names, offsets_y, lengths_y, title="", xlabel="", color='black', background='#f2f2f2') -> None:
    plt.rcdefaults()
    plt.rcParams.update({'font.size': 18})
    fig, ax = plt.subplots(figsize=(16, 7))

    # Example data
    y_pos = np.arange(len(bar_names))

    fill_left = [x + y for x, y in zip(offsets_y, lengths_y)]
    fill_left_max = max(fill_left) + 1
    fill_val = [fill_left_max - x for x in fill_left]

    p1 = ax.barh(y_pos, offsets_y, color=background, zorder=0)
    p2 = ax.barh(y_pos, lengths_y, left=offsets_y, color='white', edgecolor=color, hatch="\\\\", zorder=1, lw=2)
    p3 = ax.barh(y_pos, fill_val, left=fill_left, color=background, zorder=0)

    ax.set_yticks(y_pos)
    ax.set_yticklabels(bar_names)
    ax.invert_yaxis()  # labels read top-to-bottom
    ax.set_xlabel(xlabel)
    ax.set_title(title)

    ax.legend((p1[0], p2[0]), ('Best Effort', 'Real-Time'), loc='upper right')
    plt.show()


def plot_pk_bars(data: dict) -> None:
    plt.rcdefaults()
    plt.rcParams.update({'font.size': 18})
    fig, ax = plt.subplots(figsize=(15, 3))

    bar_names = data["x"]
    pk_rec = data["y"]["pk_rec"]
    pk_send = data["y"]["pk_send"]

    y_pos = np.arange(len(bar_names))

    p2 = ax.barh(y_pos, 1, left=pk_rec, color='white', edgecolor=data["y"]["pk_rec_color"], hatch="\\\\", zorder=1, lw=2)

    p3 = ax.barh(y_pos, 1, left=pk_send, color='white', edgecolor=data["y"]["pk_send_color"], hatch="x", zorder=1, lw=2)

    ax.set_yticks(y_pos)
    ax.set_yticklabels(bar_names)
    ax.invert_yaxis()  # labels read top-to-bottom
    ax.set_xlabel(data["xlabel"])
    ax.set_title(data["title"])

    ax.legend((p2[0], p3[0]), ('Receive', 'Send'), loc='upper right')

    plt.show()


def scale_data(in_data: list, factor: int) -> list:
    return [x * factor for x in in_data]


def plot_dicts(data_x: [], data_y: dict, xlabel=None, ylabel=None, title=None, scale="log", ylim=None,
               flags=None) -> None:
    if flags is None:
        flags = []
    plt.rcParams.update({'font.size': 20})
    for data_line in data_y:
        plt.plot(data_x, data_y[data_line]["data"],
                 linestyle=Util.get_or_default(data_y[data_line], "line_style"),
                 marker=Util.get_or_default(data_y[data_line], "marker"),
                 color=Util.get_or_default(data_y[data_line], "color"),
                 label=data_line,
                 markersize=9, linewidth=2)

    ax = plt.gca()
    ax.set_xticks(np.arange(0, max(data_x) + 1, 2))
    ax.set_xticks(np.arange(0, max(data_x) + 1, 1), minor=True)
    if ylim:
        ax.set_ylim(ylim)

    plt.yscale(scale)
    fig = plt.gcf()
    fig.set_size_inches(18, 7)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.legend()
    if "grid_y_minor" in flags:
        plt.grid(b=True, which='minor', axis='y')
    if "grid_x_minor" in flags:
        plt.grid(b=True, which='minor', axis='x')
    plt.grid()
    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    # parser.add_argument("--out", "-o", help="output file", metavar="FILE", required=True)
    # parser.add_argument("--length", "-l", help="chain length", type=int, required=True)
    parser.add_argument("--imput", "-i", help="input json file", metavar="FILE", required=True)

    args = parser.parse_args()
    dir_path = os.path.dirname(os.path.realpath(__file__))

    data = Util.read_from_json(args.imput)

    if "mode" in data:
        mode = data["mode"]
    else:
        mode = ""

    if mode == "schedule":
        color = "black"
        if "color" in data:
            color = data["color"]
        background = '#f2f2f2'
        if 'background' in data:
            background = data['background']

        plot_bars(data["x"], data["y"]["offset"], data["y"]["length"], xlabel=data["xlabel"], title=data["title"],
                  color=color, background=background)

    elif mode == "schedule_mod":
        plot_multiple_bars(data["x"], data["y"], xlabel=data["xlabel"], title=data["title"])

    elif mode == "pk_bars":
        plot_pk_bars(data)

    elif mode == "err_bars":
        scale = "log"
        if "scale" in data:
            scale = data["scale"]
        ylim = None
        if "ylim" in data:
            ylim = data["ylim"]
        flags = None
        if "flags" in data:
            flags = data["flags"]
        plot_error_bars(data["x"], data["y"],
                        xlabel=data["xlabel"], ylabel=data["ylabel"], title=data["title"], scale=scale, ylim=ylim, flags=flags)

    else:
        ylim = None
        if "ylim" in data:
            ylim = data["ylim"]

        scale = "log"
        if "scale" in data:
            scale = data["scale"]

        flags = None
        if "flags" in data:
            flags = data["flags"]

        scaled_data = data["y"]
        if "yfac" in data:
            for flow in data["y"]:
                scaled_data[flow]["data"] = scale_data(data["y"][flow]["data"], data["yfac"])

        plot_dicts(data["x"], scaled_data,
                   xlabel=data["xlabel"],
                   ylabel=data["ylabel"],
                   title=data["title"], scale=scale, ylim=ylim, flags=flags)
