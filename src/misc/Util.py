import json


def write_as_json(data: any, out_file_path: str, intend=2):
    tmp = open(out_file_path, "w+")
    tmp.write("")
    tmp.close()

    target_file = open(out_file_path, "a")
    result_json = json.dumps(data, indent=intend)
    target_file.write(result_json)


def read_from_json(json_file: str) -> any:
    file = open(json_file, "r")
    contents = file.read()
    return json.loads(contents)


def get_or_default(data: dict, key: str, default=None) -> any:
    if key and key in data:
        return data[key]
    else:
        return default
