#!/usr/bin/env python3
import Util
import argparse
import os


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--out", "-o", help="output file", metavar="FILE", required=True)
    parser.add_argument("--length", "-l", help="chain length", type=int, required=True)

    args = parser.parse_args()
    dir_path = os.path.dirname(os.path.realpath(__file__))

    switch_idx = 4

    topology = Util.read_from_json(f"{dir_path}/base-topology.json")

    topology["topology"]["switches"].append("s1")
    switch_a = "s2"
    switch_b = "s3"
    topology["topology"]["switches"].append(switch_a)
    topology["topology"]["switches"].append(switch_b)

    topology["topology"]["links"].append(["h1", "s1"])
    topology["topology"]["links"].append(["s1", "s2"])
    topology["topology"]["links"].append(["s1", "s3"])

    for i in range(1, args.length):
        new_switch_a = f"s{switch_idx}"
        switch_idx += 1
        new_switch_b = f"s{switch_idx}"
        switch_idx += 1
        topology["topology"]["switches"].append(new_switch_a)
        topology["topology"]["switches"].append(new_switch_b)

        topology["topology"]["links"].append([switch_a, new_switch_a])
        topology["topology"]["links"].append([switch_b, new_switch_b])

        switch_a = new_switch_a
        switch_b = new_switch_b

    last_switch = f"s{switch_idx}"
    topology["topology"]["switches"].append(last_switch)

    topology["topology"]["links"].append([switch_a, last_switch])
    topology["topology"]["links"].append([switch_b, last_switch])
    topology["topology"]["links"].append([last_switch, "h2"])

    Util.write_as_json(topology, args.out)