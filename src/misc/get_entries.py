#!/usr/bin/env python3


import argparse
import os
import re

import matplotlib.pyplot as plt
import numpy as np

import Util


def get_order(dir_name: str) -> int:
    reg = re.match("^(\\d+)\\S*$", dir_name)
    if reg:
        return int(reg.group(1))
    else:
        return 0


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", "-i", help="input dir", metavar="DIR", required=True)
    parser.add_argument("--stat-name", help="name of stat to extract", required=True)
    parser.add_argument("--stat-source", help="name of stat to extract", required=False, default="e2e")
    args = parser.parse_args()

    dir_path = os.path.dirname(os.path.realpath(__file__))

    # data = Util.read_from_json(args.imput)
    dirs: list = next(os.walk(args.input))[1]
    dirs.sort()

    test = [(get_order(x), x) for x in dirs]
    test.sort(key=lambda tup: tup[0])

    last = dirs.pop()
    dirs.insert(1, last)

    result = {}

    for _, _dir in test:
        path = f"{args.input}/{_dir}/result/result.json"
        if not os.path.isfile(path):
            path = f"{args.input}/{_dir}/result.json"
        if os.path.isfile(path):
            data = Util.read_from_json(path)
            mode = "e2e"
            if args.stat_source:
                mode = args.stat_source

            if mode == "e2e":
                streams = data["streams"]
                for stream in streams:
                    if stream not in result:
                        result[stream] = []
                    result[stream].append(data["streams"][stream]["latency_stats"][args.stat_name])

            elif mode == "ql" or mode == "qt":
                switches = data["switches"]
                for switch in switches:
                    ports = data["switches"][switch]
                    for port in ports:
                        queues = data["switches"][switch][port]
                        for queue in queues:
                            id = f"{switch}-{port}-{queue}"
                            if id not in result:
                                result[id] = []
                            if mode ==  "ql":
                                result[id].append(data["switches"][switch][port][queue]["queueLength"][args.stat_name])
                            else:
                                result[id].append(data["switches"][switch][port][queue]["queueingTime"][args.stat_name])

    for result_entry in result:
        print(result_entry)
        print(result[result_entry])
