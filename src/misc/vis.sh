#!/usr/bin/env bash

SCRIPT=$(readlink -f "$0")
script_path=$(dirname "$SCRIPT")
if test -f "$script_path/../../venv/bin/activate"
then
    echo "found virtual env!"
else
    echo "virtual env not found, installing it now!"
    "$script_path/../../install.sh" || exit 1
fi

source "$script_path/../../venv/bin/activate" || exit 1

"$script_path/vis.py" "$@"
