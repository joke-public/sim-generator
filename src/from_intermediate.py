#!/usr/bin/env python3
import os
import argparse
import time
import shutil
import util

from colorama import Fore
from colorama import Style


SIM_ENV_PATH = "/../simulation-env/"
GEN_PATH = "/gen/Main.py"
RESULT_ANALYSIS_PATH = "/result-analysis/Main.py"
DEFAULT_OUT_PATH = "/../out"
PLOTTER_MAIN_PATH = "/plotter_main.py"


def main(input_dir: str, config: str, out_dir=None, gui=False, start_plotter=False, topology=None,
         simulation_name=None, no_raw=False):
    start_time = time.time()
    dir_path = os.path.dirname(os.path.realpath(__file__))
    # simulation run name, so that multiple runs have separate directories
    if not simulation_name:
        simulation_name = util.random_str(10)

    if topology:
        util.ensure_is_file(topology)
    util.ensure_is_dir(input_dir)
    util.ensure_is_file(config)

    print(f"{Fore.GREEN}Welcome {Fore.YELLOW}to {Fore.CYAN}the {Fore.BLUE} sim-gen!{Style.RESET_ALL}")
    print(f"{Fore.GREEN}This runs name: {simulation_name}{Style.RESET_ALL}")

    if not out_dir:
        out_dir = f"{dir_path}{DEFAULT_OUT_PATH}/{simulation_name}"

    network_package = f"tsn_sim.simulations.generated.{simulation_name}"
    gen_out_dir = f"{dir_path}{SIM_ENV_PATH}/tsn-sim/simulations/generated/{simulation_name}"

    # parser_out_dir = f"{out_dir}/parser"
    result_out_dir = f"{out_dir}"

    # generate the simulation from the intermediate files
    print(f"{Fore.GREEN}Generating sim to {gen_out_dir}!{Style.RESET_ALL}")
    if os.system(f"{dir_path}{GEN_PATH} -c {config} -t {input_dir}/network.json "
                 f"-o {gen_out_dir} -tas {input_dir}/tas.json -r {input_dir}/routing.json "
                 f"--flows {input_dir}/flows.json --vid_map {input_dir}/vid-map.json "
                 f"--package {network_package}"):
        exit(1)

    if gui:
        gui_str = "gui"
    else:
        gui_str = "cmd"

    # run the simulation
    if os.system(f"{dir_path}{SIM_ENV_PATH}/tsn-sim/run-generated-subdir.sh {gen_out_dir}/sim.ini {gui_str}"):
        exit(1)
    print(f"{Fore.GREEN}Simulation finished{Style.RESET_ALL}\n")

    result_vec = f"{gen_out_dir}/results/General-#0.vec"

    if no_raw:
        no_raw_flag = "--no-raw"
    else:
        no_raw_flag = ""

    # run the result parser
    print(f"{Fore.GREEN}Analysing simulation results{Style.RESET_ALL}")
    if os.system(f"{dir_path}{RESULT_ANALYSIS_PATH} -i {result_vec} -o {result_out_dir} "
                 f"-t {topology} -s {no_raw_flag}"):
        exit(1)

    print(f"{Fore.GREEN}Removing simulation from {gen_out_dir}!{Style.RESET_ALL}")
    shutil.rmtree(gen_out_dir)

    util.print_time_elapsed(start_time, "Whole run took")

    if start_plotter:
        if os.system(f"{dir_path}{PLOTTER_MAIN_PATH} -i {result_out_dir}/result_raw.json"):
            exit(1)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--topology", "-t", help="target network topology", metavar="FILE", required=False)
    parser.add_argument("--input", "-i", help="directory containing tas.json, network.json, routing.json, "
                                              "flows.json, vid-map.json", metavar="DIR", required=True)
    parser.add_argument("--config", "-c", help="config.ini file", metavar="FILE", required=True)
    parser.add_argument("--out", "-o", help="output directory", metavar="DIR")
    parser.add_argument("--name", help="simulation name")
    parser.add_argument("--start-plotter", "-p", help="if the plotter should be started after the sim is finished",
                        action='store_true')
    parser.add_argument("--gui", help="open simulation in graphical mode", action='store_true')
    parser.add_argument("--no-raw", help="doesn't save raw date", action='store_true')

    args = parser.parse_args()
    main(args.input, args.config, out_dir=args.out, gui=args.gui, start_plotter=args.start_plotter,
         simulation_name=args.name, topology=args.topology, no_raw=args.no_raw)
