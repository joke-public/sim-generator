#!/usr/bin/env bash
echo -e "\e[32mStarting simulation\e[0m"
ini_path="$(readlink $1 -e FILE)"
if [[ -z "$ini_path" ]]
then
    echo -e "\e[31mNo .ini File to simulate has been specified, aborting\e[0m"
    exit 1
fi
echo -e "\e[32mFound config file at $ini_path\e[0m"
echo "$ini_path"
cd `dirname $0`
cd simulations/generated
../../out/gcc-release/src/tsn-sim -m -u Qtenv -n ..:../../src:../../../nesting/simulations:../../../nesting/src:../../../inet/src:../../../inet/examples:../../../inet/tutorials:../../../inet/showcases --image-path=../../../inet/images -l ../../../nesting/src/nesting -l ../../../inet/src/INET "$ini_path"
