#!/bin/bash

NESTING="../../../../nesting"
INET="$NESTING/../inet"
MODE="Cmdenv"

if [[ -z $1 ]]
then
        echo -e "\e[31mError: target simulation ini file not set\e[0m"
        exit 1
fi

if [[ $2 == "gui" ]]
then
    MODE="Qtenv"
fi

cd `dirname "$1"`
echo -e "\e[32mRunning simulation $1\e[0m"
../../../out/gcc-release/src/tsn-sim -m -u "$MODE" -n ../..:$NESTING/src:$INET/src -l $NESTING/src/nesting -l $INET/src/INET "$(basename $1)"
