#!/usr/bin/env bash

cd `dirname $0`

type omnetpp && echo -e "\e[32mfound omnetpp in path\e[0m" || ( echo "omnetpp not found in path, aborting!" && exit 1 )

target="../inet"
if find "$target" -mindepth 1 -print -quit 2>/dev/null | grep -q .; then
    echo "found inet source code!"
else
    echo "inet source code not found, running setup"
    git submodule init || exit 1
    git submodule update || exit 1
fi

echo -e "\e[32mbuilding project dependency: inet\e[0m"
if [[ -e "../inet/src/libINET.so" ]]
then
    echo "already build, skipping rebuild"
else
    cd ../inet
    make makefiles || exit 1
    make -j4 all || exit 1
fi

echo -e "\e[32mbuilding project dependency: nesting\e[0m"
if [[ $1 == "no-rebuild" ]]
then
    if [[ -e "../nesting/src/libnesting.so" ]]
    then
        echo "already build, skipping rebuild"
        echo -e "\e[31mWARNING, if you have changes in the nesting library, this won't be visible until you run make clean in the nesting dir!\e[0m"
    else
        cd ../nesting
        make makefiles || exit 1
        make -j4 all || exit 1
    fi
else
    cd ../nesting
    make makefiles || exit 1
    make clean || exit 1
    make -j4 all || exit 1
fi

echo -e "\e[32mbuilding tsn-sim\e[0m"
cd ../tsn-sim
make makefiles || exit 1
make clean || exit 1
make -j4 all || exit 1