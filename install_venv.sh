#!/bin/bash

SCRIPT=$(readlink -f "$0")
script_path=$(dirname "$SCRIPT")
cd "$script_path"

echo -e "\e[32mSetting up virtual env (venv)\e[0m"

pip3 install virtualenv --user || pip3 install virtualenv || exit 1

virtualenv venv --python=python3 || exit 1

source venv/bin/activate || exit 1

pip3 install -r requirements.txt || exit 1

deactivate || exit 1

echo -e "\e[32mSuccess!\e[0m"
echo "start using it by sourcing venv/bin/activate"
