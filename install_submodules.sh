#!/bin/bash

SCRIPT=$(readlink -f "$0")
script_path=$(dirname "$SCRIPT")
cd "$script_path"

echo -e "\e[32mInstalling submodules\e[0m"

./simulation-env/tsn-sim/build.sh

echo -e "\e[32mSuccess!\e[0m"
