#!/bin/bash

SCRIPT=$(readlink -f "$0")
script_path=$(dirname "$SCRIPT")
cd "$script_path"

echo "updating submodules"

cd simulation-env/nesting
git pull origin master
cd ../..

./install.sh

