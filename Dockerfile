FROM jfkhatesdocker/omnetpp_env:latest

# install github.com host fingerprint
# RUN mkdir -p ~/.ssh && touch ~/.ssh/known_hosts && echo $(ssh-keyscan github.com) >> ~/.ssh/known_hosts

WORKDIR /home/sim-generator

# uncomment this line and comment the next three commands, to use the files from the local directory instead of fresh clones
# also change the .dockerignore
# COPY simulation-env simulation-env
RUN mkdir simulation-env

RUN cd simulation-env/ && git clone --branch v4.1.0 --depth 1 https://github.com/inet-framework/inet.git

RUN cd simulation-env/ && git clone https://gitlab.com/joke-public/nesting.git

RUN cd simulation-env/inet && make makefiles && make -j8

RUN cd simulation-env/nesting && make makefiles && make -j8

RUN pip3 install virtualenv

COPY simulation-env/tsn-sim simulation-env/tsn-sim

RUN cd simulation-env/tsn-sim && make makefiles && make -j8

COPY requirements.txt *.sh ./

RUN ./install.sh

COPY examples examples

COPY src src

COPY README.md LICENSE ./
