#!/bin/bash

SCRIPT=$(readlink -f "$0")
script_path=$(dirname "$SCRIPT")
cd "$script_path"

./install_submodules.sh

./install_venv.sh

